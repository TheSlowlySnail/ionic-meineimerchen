import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { StorageProvider} from '../../providers/storage/storage';
import { Platform } from 'ionic-angular';



@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  
  test:any = "";
  constructor(public navCtrl: NavController,public restProvider: RestProvider, public storage: StorageProvider, public plt: Platform) {
    this.getDatas();

    this.myGen.next();
    this.myGen.next();
    //this.myGen.next();
    
    
    
    console.log("HomePage "+this.test);

  }
  *createGenerator() {
    yield this.storage.save("winden");
    yield this.plt.ready().then(()=>{
      this.test = this.storage.load();
    })
     
    //yield this.test = this.storage.trashData;
    
  }

  myGen = this.createGenerator();

  datas:any;

  getDatas(){
    this.restProvider.getJson("gutach")
    .subscribe(data=> this.datas=data);
  }

}
