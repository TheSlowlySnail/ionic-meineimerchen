export class Ort {
    private id:string;
    private name:string;
    private arten:any;

    public setId(id:string){
        this.id = id;
    }
    public getId(){
        return this.id;
    }

    public setName(name:string){
        this.name = name;
    }
    public getName(){
        return this.name;
    }

    public setArten(arten:any){
        this.arten = arten;

    }

    public getArten(){
        return this.arten;
    }

}