export class Art{
    private name:string;
    private date:Date;
    private location:string;
    private description: string;

    public getName(){
        return this.name;
    }

    public setName(name:string){
        this.name = name;
    }

    public getDate(){
        return this.date;
    }

    public setDate(date:Date){
        this.date = date;

    }

    public getLocation(){
        return this.location;
    }

    public setLocation(){
        this.location = this.location;
    }

    public getDescription():string{
        return this.description
    }


}