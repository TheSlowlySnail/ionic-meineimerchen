import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RestProvider {

  apiUrl = 'http://meiering.de/json/winden.json';

  constructor(public http: HttpClient) {
    console.log('Hello RestProvider Provider');

  }

  getJson(ort:string) {

    
    return this.http.get('assets/'+ ort +'.json');
  }

}
