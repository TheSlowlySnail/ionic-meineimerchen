import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';

import { NativeStorage } from '@ionic-native/native-storage';
//import { IonicStorageModule } from '@ionic/Storage';

/*
  Generated class for the StorageProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class StorageProvider{
  private isSet:boolean;
  public key : string = "town";
  public trashData:string = this.load();


  constructor(private nativeStorage: NativeStorage, private plt:Platform) {
    console.log('Hello StorageProvider Provider');
    
    //this.save("gutach");
    //this.load();
   // console.log(this.load());
    
  }

  public save( value){
    console.log("StorageProvider: save(" + value + ")");
    this.nativeStorage.setItem(this.key,value);
  }

  public load():string{
    var dataString:string ="";
    this.plt.ready().then(
      ()=>{

        this.nativeStorage.getItem(this.key)
          .then(data => {
            dataString = data;
            console.log("StorageProvider: "+dataString);
            console.log("StorageProvider: load('gutach')"+data);      
            
          })
          .catch(
            ()=> {return "false";}
          );

      }
    );
      return dataString;
      
  }

  
 /**
  public save(value){
    this.storage.setItem(this.key,value);
  }

   * load
   public load():string {
     return this.storage.getItem(this.key);
    }
    
    */
  

}
